<?php

/**
 * Dump and Die
 *

 * @param  dynamic  mixed
 * @return void
 */
function dd()
{
    echo '<pre>';
    array_map(function($x) { var_dump($x); }, func_get_args());
    die;
}

/**
 * Pretty Print
 *

 * @param  dynamic  mixed
 * @return void
 */
function pp()
{
    echo '<pre>';
    array_map(function($x) { print_r($x); }, func_get_args());
}



