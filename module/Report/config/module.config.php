<?php

namespace Report;

use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'report' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\ReportController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],
    'view_manager' => [
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
        ],
        'template_path_stack' => [
            'report' => __DIR__ . '/../view'
        ],
    ],
];
