<?php
namespace Report\Controller;

use Report\Service\ReportServiceInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ReportController extends AbstractActionController
{
    private $service;

    public function __construct(ReportServiceInterface $service)
    {
        $this->service = $service;
    }

    public function indexAction()
    {
        $result = $this->service->getDashboard();

        return new ViewModel($result);
    }
}

