<?php
/**
 * Created by PhpStorm.
 * User: Pavel.Shalev
 * Date: 11/2/2018
 * Time: 8:16 PM
 */

namespace Report;

use Report\Service\ReportService;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                Model\UserTable::class => function($container) {
                    $tableGateway = $container->get(Model\UserTableGateway::class);
                    $adapter = $container->get(AdapterInterface::class);

                    return new Model\UserTable($tableGateway, $adapter);
                },
                Model\UserTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\User());

                    return new TableGateway('user', $dbAdapter, null, $resultSetPrototype);
                },

                Model\SessionTable::class => function($container) {
                    $tableGateway = $container->get(Model\SessionTableGateway::class);
                    $adapter = $container->get(AdapterInterface::class);

                    return new Model\SessionTable($tableGateway, $adapter);
                },
                Model\SessionTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Session());

                    return new TableGateway('session', $dbAdapter, null, $resultSetPrototype);
                },

                Model\ViewerTable::class => function($container) {
                    $tableGateway = $container->get(Model\ViewerTableGateway::class);
                    $adapter = $container->get(AdapterInterface::class);

                    return new Model\ViewerTable($tableGateway, $adapter);
                },
                Model\ViewerTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Viewer());

                    return new TableGateway('viewer', $dbAdapter, null, $resultSetPrototype);
                },

                Service\ReportService::class => function ($container) {
                    $viewerTable = $container->get(Model\ViewerTable::class);

                    return new ReportService($viewerTable);
                },
            ],
        ];
    }

    public function getControllerConfig()
    {
        return [
            'factories' => [
                Controller\ReportController::class => function($container) {
                    return new Controller\ReportController(
                        $container->get(Service\ReportService::class)
                    );
                },
            ],
        ];
    }
}
