<?php

namespace Report\Model;

interface ViewerTableInterface
{
    public function fetchAll();

    public function getTotalNumberOfViews();

    public function getUniqueViewers();

    public function getTopViewers();

    public function getViewersBrowsersUsed();

    public function getMostPopularSessions();
}


