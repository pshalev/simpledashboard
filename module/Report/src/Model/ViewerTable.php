<?php

namespace Report\Model;

use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Adapter\Driver\Pdo\Pdo;
use Zend\Db\TableGateway\TableGatewayInterface;

class ViewerTable implements ViewerTableInterface
{
    private $tableGateway;
    private $adapter;

    public function __construct(TableGatewayInterface $tableGateway, AdapterInterface $adapter)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter = $adapter;
    }

    public function fetchAll()
    {
        return $this->tableGateway->select();
    }

    public function getTotalNumberOfViews()
    {
        $sql = '
            SELECT
                count(v.id) AS viewsNumber
            FROM
                viewer AS v
        ';

        $statement = $this->adapter->query($sql)->execute();
        $currentRow = $statement->current();
        $res = $currentRow['viewsNumber'];

        return $res;
    }

    public function getUniqueViewers()
    {
        $sql = '
            SELECT
                COUNT(DISTINCT v.session_id, v.user_id) AS uniqueViewers
            FROM
                viewer AS v
        ';

        $statement = $this->adapter->query($sql)->execute();
        $currentRow = $statement->current();
        $res = $currentRow['uniqueViewers'];

        return $res;
    }

    public function getTopViewers()
    {
        $sql = '
            SELECT
                CONCAT(u.firstname, \' \', u.lastname) AS fullName,
                COUNT(v.id) AS countOfViews
            FROM
                viewer AS v
                INNER JOIN user AS u
                    ON (u.id = v.user_id)
            GROUP BY
                u.id
            ORDER BY
                countOfViews DESC
            LIMIT 5
        ';

        $statement = $this->adapter->query($sql)->execute();

        $res = array();
        foreach ($statement as $row) {
            $res[] = $row;
        }

        return $res;
    }

    public function getViewersBrowsersUsed()
    {
        $sql = '
            SELECT
                (CASE
                    WHEN v.http_user_agent LIKE \'%Firefox/%\' THEN \'Firefox\'
                    WHEN v.http_user_agent LIKE \'%Chrome/%\' THEN \'Chrome\'
                    WHEN v.http_user_agent LIKE \'%MSIE %\' THEN \'Internet Explorer\'
                    WHEN v.http_user_agent LIKE \'%Opera%\' THEN \'Opera\'
                    WHEN v.http_user_agent LIKE \'%Safari%\' THEN \'Safari\'
                    ELSE \'Unknown\'
                  END) AS browser,
                  COUNT(v.id) AS browserCount
            FROM
                viewer AS v
            GROUP BY
                browser
            ORDER BY
                browserCount DESC
            LIMIT 5
        ';

        $statement = $this->adapter->query($sql)->execute();

        $res = array();
        foreach ($statement as $row) {
            $res[] = $row;
        }

        return $res;
    }

    public function getMostPopularSessions()
    {
        $sql = '
            SELECT
                s.title,
                COUNT(v.id) AS countOfViews
            FROM
                viewer AS v
                INNER JOIN session AS s
                    ON (s.id = v.session_id)
            GROUP BY
                s.id  
            ORDER BY
                countOfViews DESC
            LIMIT 5
        ';

        $statement = $this->adapter->query($sql)->execute();

        $res = array();
        foreach ($statement as $row) {
            $res[] = $row;
        }

        return $res;
    }
}


