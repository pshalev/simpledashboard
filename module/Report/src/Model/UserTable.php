<?php

namespace Report\Model;

use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\TableGateway\TableGatewayInterface;

class UserTable
{
    private $tableGateway;
    private $adapter;

    public function __construct(TableGatewayInterface $tableGateway, AdapterInterface $adapter)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter = $adapter;
    }

    public function fetchAll()
    {
        return $this->tableGateway->select();
    }
}


