<?php
namespace Report\Service;

use Report\Model\UserTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

interface ReportServiceInterface
{
    public function getDashboard();

    public function getNumberOfViews();

    public function getUniqueViewers();

    public function getTopViewers();

    public function getBrowsersUsed();

    public function getMostPopularSessions();
}

