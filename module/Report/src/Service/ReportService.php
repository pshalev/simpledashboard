<?php
namespace Report\Service;

use Report\Model\ViewerTableInterface;

class ReportService implements ReportServiceInterface
{
    private $viewersTable;

    public function __construct(ViewerTableInterface $viewersTable)
    {
        $this->viewersTable = $viewersTable;
    }

    public function getDashboard()
    {
        $numberOfViews = $this->getNumberOfViews();
        $uniqueViews = $this->getUniqueViewers();
        $topViewers = $this->getTopViewers();
        $browsers = $this->getBrowsersUsed();
        $mostPopularSessions = $this->getMostPopularSessions();

        $result = array(
            'numberOfViews' => $numberOfViews,
            'uniqueViews' => $uniqueViews,
            'topViewers' => $topViewers,
            'browsers' => $browsers,
            'mostPopularSessions' => $mostPopularSessions,
        );

        return $result;
    }

    public function getNumberOfViews()
    {
        try {
            $totalNumberOfViews = $this->viewersTable->getTotalNumberOfViews();

            if (is_null($totalNumberOfViews) || ! is_numeric($totalNumberOfViews)) {
                throw new \Exception('Something went wrong getting number of views from DB!');
            }
        } catch (\Exception $e) {
            //TODO Handle ex
            $totalNumberOfViews = 0;
        }

        return $totalNumberOfViews;
    }

    public function getUniqueViewers()
    {
        try {
            $uniqueViews = $this->viewersTable->getUniqueViewers();
            if (is_null($uniqueViews ) || ! is_numeric($uniqueViews )) {
                throw new \Exception('Something went wrong getting number of unique views from DB!');
            }
        } catch (\Exception $e) {
            //TODO Handle ex
            $uniqueViews = 0;
        }

        return $uniqueViews;
    }

    public function getTopViewers()
    {
        $topViewers = $this->viewersTable->getTopViewers();

        return $topViewers;
    }

    public function getBrowsersUsed()
    {
        $browsersUsed = $this->viewersTable->getViewersBrowsersUsed();

        return $browsersUsed;
    }

    public function getMostPopularSessions()
    {
        $mostPopularSessions = $this->viewersTable->getMostPopularSessions();

        return $mostPopularSessions;
    }
}

